package sweeper

import (
	"context"
	"crypto/ecdsa"
	"errors"
	"fmt"
	"math/big"
	"strings"
	"time"

	"github.com/rs/zerolog/log"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/params"
	"gitlab.com/arczi929/eth-sweeper-bot/internal/client"
	"gitlab.com/arczi929/eth-sweeper-bot/internal/config"
)

func Execute(ctx context.Context, config *config.Network) error {
	// Create go-ethereum client based on configuration
	cl, err := client.CreateClient(ctx, config)
	if err != nil {
		return err
	}

	chainID, _ := cl.ChainID(ctx)

	destAddr := common.HexToAddress(config.DestAddr)
	minToSweep := new(big.Float)
	minToSweep.SetString(config.MinSweep)

	privateKey, err := crypto.HexToECDSA(config.SweepPrivateKey)
	if err != nil {
		return err
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return errors.New("casting public key to ECDSA failed")
	}

	fromAddr := crypto.PubkeyToAddress(*publicKeyECDSA)

	checkCount := 0
	doneCount := 0

	for {
		checkCount++

		// Balance from latest known block
		balance, err := cl.BalanceAt(ctx, fromAddr, nil)
		if err != nil {
			return err
		}

		log.Info().
			Int("run", checkCount).
			Int("done", doneCount).
			Str("network", config.Name).
			Msg("Checking")

		switch balance.Cmp(etherToWei(minToSweep)) {
		case 1:
			nonce, err := cl.PendingNonceAt(ctx, fromAddr)
			if err != nil {
				return err
			}

			txAmount := new(big.Int)
			gasPrice := new(big.Int)
			gasPrice.SetInt64(int64(config.GasGwei) * 1000000000)
			txAmount.SetInt64(balance.Int64() - gasPrice.Int64())

			tx := types.NewTransaction(nonce+1, destAddr, txAmount, uint64(21000), gasPrice, nil)
			signedTx, err := types.SignTx(tx, types.NewEIP155Signer(chainID), privateKey)
			if err != nil {
				return err
			}

			err = cl.SendTransaction(context.Background(), signedTx)
			if err != nil {
				return err
			}
			doneCount++
		case 0:
		case -1:
			b, accuracy := weiToEther(balance).Float64()
			log.Warn().
				Str("network", config.Name).
				Float64("balance", b).
				Str("accuracy", accuracy.String()).
				Msg("Balance is lower than minimum amount to sweep")
		}

		time.Sleep(25 * time.Second)
	}
}

func etherToWei(eth *big.Float) *big.Int {
	truncInt, _ := eth.Int(nil)
	truncInt = new(big.Int).Mul(truncInt, big.NewInt(params.Ether))
	fracStr := strings.Split(fmt.Sprintf("%.18f", eth), ".")[1]
	fracStr += strings.Repeat("0", 18-len(fracStr))
	fracInt, _ := new(big.Int).SetString(fracStr, 10)
	wei := new(big.Int).Add(truncInt, fracInt)
	return wei
}

func weiToEther(wei *big.Int) *big.Float {
	f := new(big.Float)
	f.SetPrec(236) //  IEEE 754 octuple-precision binary floating-point format: binary256
	f.SetMode(big.ToNearestEven)
	fWei := new(big.Float)
	fWei.SetPrec(236) //  IEEE 754 octuple-precision binary floating-point format: binary256
	fWei.SetMode(big.ToNearestEven)
	return f.Quo(fWei.SetInt(wei), big.NewFloat(params.Ether))
}
