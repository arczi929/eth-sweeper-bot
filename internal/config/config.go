package config

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
)

type Config struct {
	Network map[string]*Network `yaml:"network"`
}

type Network struct {
	Name            string
	NodeUrl         string `yaml:"node_url"`
	SweepAddr       string `yaml:"sweep_wallet_address"`
	SweepPrivateKey string `yaml:"sweep_wallet_private_key"`
	DestAddr        string `yaml:"wallet_dest_address"`
	GasGwei         int    `yaml:"eth_gas_gwei"`
	MinSweep        string `yaml:"eth_min_sweep"`
}

func NewConfig(path string) (*Config, error) {
	config := &Config{}

	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	d := yaml.NewDecoder(file)

	if err := d.Decode(&config); err != nil {
		return nil, err
	}

	return config, nil
}

func (c *Config) ConfiguredNetworks() []string {
	i := 0
	networks := make([]string, len(c.Network))
	for k := range c.Network {
		networks[i] = k
		i++
	}

	return networks
}

func (c *Config) NetworkConfig(name string) (*Network, error) {
	cfg := c.Network[name]
	if cfg == nil {
		return nil, fmt.Errorf("network %s not found in configuration", name)
	}
	cfg.Name = name
	return cfg, nil
}
