package client

import (
	"context"

	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/rs/zerolog/log"
	"gitlab.com/arczi929/eth-sweeper-bot/internal/config"
)

type Network string

func CreateClient(ctx context.Context, network *config.Network) (*ethclient.Client, error) {
	cl, err := ethclient.Dial(network.NodeUrl)
	if err != nil {
		log.Error().Err(err)
	}

	cid, _ := cl.ChainID(ctx)

	log.Debug().Str("network", string(network.Name)).Str("chainId", cid.String()).Msg("Client connected")

	return cl, nil
}
