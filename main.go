package main

import (
	"context"
	"sync"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/arczi929/eth-sweeper-bot/internal/config"
	"gitlab.com/arczi929/eth-sweeper-bot/internal/sweeper"
)

func main() {
	ctx := context.Background()
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	config, err := config.NewConfig("config.yml")
	if err != nil {
		panic(err)
	}
	networks := config.ConfiguredNetworks()

	log.Info().Int("networks", len(networks)).Any("list", networks).Msg("Starting sweepers")

	var wg sync.WaitGroup

	for _, n := range networks {
		wg.Add(1)
		go func(network string) {
			c, _ := config.NetworkConfig(n)
			err := sweeper.Execute(ctx, c)
			if err != nil {
				panic(err)
			}
			wg.Done()
		}(n)
	}

	wg.Wait()
}
